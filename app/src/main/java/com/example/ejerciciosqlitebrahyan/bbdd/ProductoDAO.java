package com.example.ejerciciosqlitebrahyan.bbdd;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

import com.example.ejerciciosqlitebrahyan.entidades.Producto;

public class ProductoDAO {

    SQLiteDatabase db;

    public ProductoDAO(Context context){
        db = new ProductoDbHelper(context).getWritableDatabase();
    }

    /**
     *  Elimina producto de la BD con el codigo deseado
     * @param codigo codigo de producto
     * @return si el producto se pudo eliminar
     */
    public boolean eliminarPorCodigo(int codigo) {

        String[] args = {Integer.toString(codigo)};

        return 0 < db.delete(
                ProductContract.ProductoEntry.TABLE_NAME,
                ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+"=?",
                args
        );
    }

    /**
     * Inserta el objeto producto en la base de datos
     * @param producto
     * @throws SQLException
     */
    public void insertarProducto(@NonNull Producto producto) throws SQLException {
        db.insertOrThrow(ProductContract.ProductoEntry.TABLE_NAME,
                null,
                producto.toContentValues());
    }

    /**
     * Devuelve un objeto producto a partir su codigo
     * @param codigo codigo del producto
     * @return objeto producto budcado
     */

    public Producto productoPorCodigo(int codigo){

        Producto productoBuscado = null;
        String[] args = {Integer.toString(codigo)};

        Cursor cursor = db.query(false,
                ProductContract.ProductoEntry.TABLE_NAME,
                null,
                ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+"=?",
                args,
                null,
                null,
                null,
                String.valueOf(1));

        if(cursor.moveToFirst()){
            int codigoProducto = cursor.getInt(1);
            double precioProducto = cursor.getDouble(2);
            String descripcionProducto = cursor.getString(3);
            productoBuscado = new Producto(codigoProducto,descripcionProducto,precioProducto);
        }
        cursor.close();
        return productoBuscado;
    }

    /**
     *  Devuelve un objeto Producto con la informacion de la BD
     * @param descripcion descripcion del producto buscado
     * @return objeto producto con la informacion de la BD
     */
    public Producto productoPorDescripcion(String descripcion) {

        Producto productoBuscado = null;
        String[] args = {descripcion};

        Cursor cursor = db.query(false,
                ProductContract.ProductoEntry.TABLE_NAME,
                null,
                ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION+"=?",
                args,
                null,
                null,
                null,
                String.valueOf(1));

        if( cursor.moveToFirst() ){
            int codigoProducto = cursor.getInt(1);
            double precioProducto = cursor.getDouble(2);
            String descripcionProducto = cursor.getString(3);
            productoBuscado = new Producto(codigoProducto,descripcionProducto,precioProducto);
        }

        cursor.close();
        return productoBuscado;
    }

    /**
     * Modifica un producto, el dato inmutable es el codigo, por ello se modifican precio y descripcion
     * @param producto producto a modificar
     * @return si al menos una fila se ha visto afectada
     */
    public boolean modificarProducto(@NonNull Producto producto){

        String[] args = {Integer.toString(producto.getCondigo())};

        return 0 < db.update(
                ProductContract.ProductoEntry.TABLE_NAME,
                producto.toContentValues(),
                ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+"=?",
                args
        );
    }
}

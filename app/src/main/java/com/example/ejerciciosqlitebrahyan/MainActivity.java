package com.example.ejerciciosqlitebrahyan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.ejerciciosqlitebrahyan.bbdd.ProductoDAO;
import com.example.ejerciciosqlitebrahyan.entidades.Producto;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etCodigo;
    EditText etDescripcion;
    EditText etPrecio;

    Button btnAlta;
    Button btnConsultaPorCodigo;
    Button btnConsultaPorDescripcion;
    Button btnBajaPorCodigo;
    Button btnModificacion;

    ProductoDAO productoDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Conexion con la base de datos
        productoDAO = new ProductoDAO(this);
        //Elementos visuales
        etCodigo = findViewById(R.id.etCodigo);
        etDescripcion = findViewById(R.id.etDescripcion);
        etPrecio = findViewById(R.id.etPrecio);

        btnAlta = findViewById(R.id.btnAlta);
        btnConsultaPorCodigo = findViewById(R.id.btnConsultaPorCodigo);
        btnConsultaPorDescripcion = findViewById(R.id.btnConsultaPorDescripcion);
        btnBajaPorCodigo = findViewById(R.id.btnBajaPorCodigo);
        btnModificacion = findViewById(R.id.btnModificacion);

        btnAlta.setOnClickListener(this);
        btnConsultaPorCodigo.setOnClickListener(this);
        btnConsultaPorDescripcion.setOnClickListener(this);
        btnBajaPorCodigo.setOnClickListener(this);
        btnModificacion.setOnClickListener(this);

    }

    @Override
    public void onClick(View click) {

        if( click.equals(btnAlta) ){

            insertarProducto();
        }

        if( click.equals(btnConsultaPorCodigo) ){
            consultarPorCodigo();
        }

        if( click.equals(btnConsultaPorDescripcion) ){
            consultarPorDescripcion();
        }

        if( click.equals(btnBajaPorCodigo) ){
            bajaPorCodigo();
        }

        if( click.equals(btnModificacion) ){
            modificar();
        }
    }

    /**
     * Modifica los campos descripcion y precio del producto con el codigo especificado
     * la tarea se realiza en un nuevo hilo de ejecucion
     */
    private void modificar() {

        if( comprobarCampos(Arrays.asList(etCodigo,etPrecio,etDescripcion)) ){

            new Thread(()->{

                int codigoProducto = Integer.parseInt(etCodigo.getText().toString());
                double precioProducto = Double.parseDouble(etPrecio.getText().toString());
                String descripcionProducto = etDescripcion.getText().toString();
                Producto producto = new Producto(codigoProducto,descripcionProducto,precioProducto);

                String mensaje = productoDAO.modificarProducto(producto) ?
                        "Producto modificado con exito" :
                        "NO se puede modificar el producto con codigo "+producto.getCondigo();
                runOnUiThread(()->{
                    mensajeToast(mensaje);
                    limpiarCampos();
                });
            }).start();
        }else {
            mensajeToast("Debe rellenar todos los campos para modificar un producto");
        }
    }

    /**
     * Borra de la base de datos el producto con el codigo especificado por el usuario en
     * el campo editable, se realiza en un hilo secundario
     */
    private void bajaPorCodigo() {

        if ( comprobarCampos(Arrays.asList(etCodigo)) ){

            new Thread(()->{
                int codigo = Integer.parseInt(etCodigo.getText().toString());
                String mensaje = productoDAO.eliminarPorCodigo(codigo) ?
                        "Producto BORRADO con exito" :
                        "No se pudo borrar el producto con codigo "+codigo;

                runOnUiThread(()->{
                    mensajeToast(mensaje);
                    limpiarCampos();
                });
            }).start();
        }else {
            mensajeToast("debe rellenar el campo codigo");
        }
    }

    /**
     * Consulta por descripcion un producto en la base de datos y muestra el resultado en los
     * campos editables, asi es mas facil editarlos o copiar su valores, si no lo encuentra
     * informa al usario por Toast y limpia los campos.
     * La consulta se realiza en un hilo nuevo para no interferir con el the UI
     */
    private void consultarPorDescripcion() {

        if( comprobarCampos(Arrays.asList(etDescripcion)) ){

            new Thread(()->{
                String descripcion = etDescripcion.getText().toString();
                Producto producto = productoDAO.productoPorDescripcion(descripcion);
                if( producto != null ){
                    runOnUiThread(()->{
                        etPrecio.setText( Double.toString(producto.getPrecio()) );
                        etCodigo.setText( Integer.toString(producto.getCondigo()) );
                    });
                }else{
                    runOnUiThread(()->{
                        mensajeToast("No encontrado producto con descripcion "+descripcion);
                        limpiarCampos();
                    });
                }
            }).start();
        }else{
            mensajeToast("debe completar el campo descripcion");
        }
    }

    /**
     * Consulta por codigo un producto en la base de datos y muestra el resultado en los
     * campos editables, asi es mas facil editarlos o copiar su valores, si no lo encuentra
     * informa al usario por Toast y limpia los campos.
     * La consulta se realiza en un hilo nuevo para no interferir con el the UI
     */
    private void consultarPorCodigo() {

        if( comprobarCampos(Arrays.asList(etCodigo)) ){

            new Thread(()->{
                int codigo = Integer.parseInt( etCodigo.getText().toString() );
                Producto producto = productoDAO.productoPorCodigo(codigo);
                if(producto != null){
                    runOnUiThread(()->{
                        etDescripcion.setText(producto.getDescripcion());
                        etPrecio.setText( Double.toString(producto.getPrecio()) );
                    });
                }else{
                    runOnUiThread(()->{
                        mensajeToast("No se econtro producto con codigo "+codigo);
                        limpiarCampos();
                    });
                }
            }).start();
        }else{
            mensajeToast("debe rellenar el campo codigo");
        }
    }

    /**
     * Inserta producto en la base de datos a partir de informacion proporcionada por el usuario,
     * lanza excepcion si ocurre error durante la insercion, si no ha rellenado todos los campos se
     * informa al usuario con mensaje toast.
     * La insercion se lleva cabo en un nuevo hilo, para no interferir con el UIThread
     */

    private void insertarProducto() {

        if( comprobarCampos(Arrays.asList(etCodigo,etPrecio,etPrecio)) ){

            new Thread(() -> {

                int codigo = Integer.parseInt(etCodigo.getText().toString());
                double precio = Double.parseDouble( etPrecio.getText().toString() );
                String descripcion = etDescripcion.getText().toString();
                Producto producto = new Producto(codigo,descripcion,precio);
                try{
                    productoDAO.insertarProducto(producto);
                    runOnUiThread(()->{
                      mensajeToast("Producto insertado en la base de datos");
                      limpiarCampos();
                    });
                }catch (SQLException sqlException){
                    runOnUiThread(()-> mensajeToast("Error al insertar el producto en la base de datos"));
                }

            }).start();

        }else{
            mensajeToast("Debe rellenar todos los campos para insertar el producto");
        }

    }

    /**
     * Limpia los campos de texto editables.
     */
    private void limpiarCampos() {
        etCodigo.setText("");
        etPrecio.setText("");
        etDescripcion.setText("");
    }

    private void mensajeToast(@NonNull String mensaje){
        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }

    /**
     * Comprueba que todos los campos pasados por parametro hayan
     * sido completados por el usuario
     * @param listaCampos lista de campos que se quiere comprobar que no esten vacios
     * @return boolean
     */

    private boolean comprobarCampos(@NonNull List<EditText> listaCampos){

        boolean camposCompletos = true;
            for(EditText campo : listaCampos){
                String contenido = campo.getText().toString();
                if ( contenido.equals("") ){
                    camposCompletos = false;
                }
            }
        return  camposCompletos;
    }
}
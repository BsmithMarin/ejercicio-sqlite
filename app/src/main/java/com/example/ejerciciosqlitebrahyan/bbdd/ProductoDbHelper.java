package com.example.ejerciciosqlitebrahyan.bbdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductoDbHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + ProductContract.ProductoEntry.TABLE_NAME + " (" +
                    ProductContract.ProductoEntry._ID + " INTEGER PRIMARY KEY," +
                    ProductContract.ProductoEntry.COLUMN_NAME_CODIGO + " INTEGER NOT NULL," +
                    ProductContract.ProductoEntry.COLUMN_NAME_PRECIO+" REAL NOT NULL,"+
                    ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION + " TEXT NOT NULL,"+
                    "UNIQUE ("+ProductContract.ProductoEntry.COLUMN_NAME_CODIGO+")," +
                    "UNIQUE ("+ProductContract.ProductoEntry.COLUMN_NAME_DESCRIPCION+"))";

    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "Producto.db";

    public ProductoDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
